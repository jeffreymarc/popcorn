<?php

use Illuminate\Database\Seeder;

class FollowsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('follows')->insert([
            'user_id_current' => 1,
            'user_id_following' => 2,
        ]);  
        DB::table('follows')->insert([
            'user_id_current' => 1,
            'user_id_following' => 4,
        ]); 
        DB::table('follows')->insert([
            'user_id_current' => 1,
            'user_id_following' => 6,
        ]); 
        
        DB::table('follows')->insert([
            'user_id_current' => 2,
            'user_id_following' => 1,
        ]);   
        DB::table('follows')->insert([
            'user_id_current' => 2,
            'user_id_following' => 5,
        ]);  
        DB::table('follows')->insert([
            'user_id_current' => 2,
            'user_id_following' => 6,
        ]);  
        
        DB::table('follows')->insert([
            'user_id_current' => 4,
            'user_id_following' => 1,
        ]);    
        DB::table('follows')->insert([
            'user_id_current' => 4,
            'user_id_following' => 2,
        ]);         
        DB::table('follows')->insert([
            'user_id_current' => 4,
            'user_id_following' => 3,
        ]);         
    }
}
