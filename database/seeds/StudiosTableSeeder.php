<?php

use Illuminate\Database\Seeder;

class StudiosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('studios')->insert([
           'name' => 'Walt Disney Pictures'
        ]);
        DB::table('studios')->insert([
           'name' => 'Warner Bros.'
        ]);
        DB::table('studios')->insert([
           'name' => 'Columbia Pictures'
        ]);
        DB::table('studios')->insert([
           'name' => '20th Century Fox'
        ]);
        DB::table('studios')->insert([
           'name' => 'Universal Pictures'
        ]);
        DB::table('studios')->insert([
           'name' => 'Paramount Pictures'
        ]);
        DB::table('studios')->insert([
           'name' => 'Marvel Studios'
        ]);
        DB::table('studios')->insert([
           'name' => 'Fox Searchlight Picture'
        ]);
        DB::table('studios')->insert([
           'name' => 'Netlfix'
        ]); 
    }
}
