<?php

use Illuminate\Database\Seeder;

class ReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reviews')->insert([
            'user_id' => 1,
            'movie_id' => 1,
            'rating' => 1,
            'reviewText' => "Explores good social problems but boring movie.",
            'created_at' => '2016-06-26 04:07:31'
        ]);
        DB::table('reviews')->insert([
            'user_id' => 2,
            'movie_id' => 1,
            'rating' => 2,
            'reviewText' => "I wasted my time watching this. I was snoring!",
            'created_at' => '2017-06-27 05:30:31'
        ]);    
        DB::table('reviews')->insert([
            'user_id' => 3,
            'movie_id' => 1,
            'rating' => 3,
            'reviewText' => "I am Fed",
            'created_at' => '2015-06-26 04:07:31'
        ]);        
        DB::table('reviews')->insert([
            'user_id' => 4,
            'movie_id' => 1,
            'rating' => 4,
            'reviewText' => "I am Tony",
            'created_at' => '2018-06-26 04:07:31'
        ]);  
        DB::table('reviews')->insert([
            'user_id' => 5,
            'movie_id' => 1,
            'rating' => 5,
            'reviewText' => "I am Bruce",
            'created_at' => '2018-06-26 04:07:31'
        ]);     
        DB::table('reviews')->insert([
            'user_id' => 6,
            'movie_id' => 1,
            'rating' => 1,
            'reviewText' => "I am Peter",
            'created_at' => '2018-06-26 04:07:31'
        ]);         
        
        DB::table('reviews')->insert([
            'user_id' => 3,
            'movie_id' => 2,
            'rating' => 4,
            'reviewText' => "Good movie about coming out.",
            'created_at' => '2018-07-01 03:56:31'
        ]);         
        
    }
}
