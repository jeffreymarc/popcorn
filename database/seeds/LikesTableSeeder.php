<?php

use Illuminate\Database\Seeder;

class LikesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('likes')->insert([
            'user_id' => 1,
            'movie_id' => 1,
            'review_id' => 1,
            'score' => 1,
        ]);      
        DB::table('likes')->insert([
            'user_id' => 2,
            'review_id' => 1,
            'score' => 1,
            'movie_id' => 1,
        ]);  
        DB::table('likes')->insert([
            'user_id' => 3,
            'review_id' => 1,
            'score' => 1,
            'movie_id' => 1,
        ]);  
        DB::table('likes')->insert([
            'user_id' => 4,
            'review_id' => 1,
            'score' => 1,
            'movie_id' => 1,
        ]);  
        DB::table('likes')->insert([
            'user_id' => 5,
            'review_id' => 1,
            'score' => 1,
            'movie_id' => 1,
        ]);  
        DB::table('likes')->insert([
            'user_id' => 6,
            'review_id' => 1,
            'score' => -1,
            'movie_id' => 1,
        ]);  
        
        
        DB::table('likes')->insert([
            'user_id' => 1,
            'review_id' => 2,
            'score' => -1,
            'movie_id' => 1,
        ]);      
        DB::table('likes')->insert([
            'user_id' => 2,
            'review_id' => 2,
            'score' => -1,
            'movie_id' => 1,
        ]);  
        DB::table('likes')->insert([
            'user_id' => 3,
            'review_id' => 2,
            'score' => -1,
            'movie_id' => 1,
        ]);  
        DB::table('likes')->insert([
            'user_id' => 4,
            'review_id' => 2,
            'score' => -1,
            'movie_id' => 1,
        ]);  
        DB::table('likes')->insert([
            'user_id' => 5,
            'review_id' => 2,
            'score' => 1,
            'movie_id' => 1,
        ]);  
        DB::table('likes')->insert([
            'user_id' => 6,
            'review_id' => 2,
            'score' => -1,
            'movie_id' => 1,
        ]);         
    }
}
