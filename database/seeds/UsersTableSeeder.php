<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
           'name' => 'Bob Jones',
           'email' => 'bob@gmail.com',
           'password' => bcrypt('123456'),
           'dob' => '01/01/1111',
           'type' => 'regular',
        ]);
        DB::table('users')->insert([
           'name' => 'Tom Synd',
           'email' => 'tom@gmail.com',
           'password' => bcrypt('123456'),
           'dob' => '01/01/1111',
           'type' => 'moderator',           
        ]);      
        DB::table('users')->insert([
           'name' => 'Fred Merc',
           'email' => 'fred@gmail.com',
           'password' => bcrypt('123456'),
           'dob' => '01/01/1111',
           'type' => 'moderator',           
        ]);    
        DB::table('users')->insert([
           'name' => 'Tony Stark',
           'email' => 'tony@gmail.com',
           'password' => bcrypt('123456'),
           'dob' => '01/01/1111',
           'type' => 'moderator',           
        ]);       
        DB::table('users')->insert([
           'name' => 'Bruce Banner',
           'email' => 'bruce@gmail.com',
           'password' => bcrypt('123456'),
           'dob' => '01/01/1111',
           'type' => 'moderator',           
        ]);      
        DB::table('users')->insert([
           'name' => 'Peter Parker',
           'email' => 'peter@gmail.com',
           'password' => bcrypt('123456'),
           'dob' => '01/01/1111',
           'type' => 'moderator',           
        ]);          
    }
}
