<?php

use Illuminate\Database\Seeder;

class PhotosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('photos')->insert([
            'id' => 1,
            'movie_id' => 1,
            'path' => 'posters/downsizing2.jpg',
            'uploader' => 'Bob Build'
        ]);
        DB::table('photos')->insert([
            'id' => 1,
            'movie_id' => 1,
            'path' => 'posters/downsizing3.jpg',
            'uploader' => 'Albert Ein'
        ]);        
    }
}
