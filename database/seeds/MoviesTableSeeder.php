<?php

use Illuminate\Database\Seeder;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('movies')->insert([
           'name' => 'Downsizing',
           'studio_id' => 6,
           'director' => 'Alexander Payne',
           'description' => "A social satire in which a man realizes he would have a better life if he were to shrink himself to five inches tall, allowing him to live in wealth and splendor.",
           'genre' => 'Drama',
           'release_date' => '22 December 2017',
           'run_time' => '2h 15 min',
           'poster' => 'posters/Downsizing.jpg',
           'trailer_url' => 'https://youtu.be/UCrBICYM0yM'
        ]);
        DB::table('movies')->insert([
           'name' => 'Love, Simon',
           'studio_id' => 4,
           'director' => 'Greg Berlanti',
           'description' => "Simon Spier keeps a huge secret from his family, his friends, and all of his classmates: he's gay. When that secret is threatened, Simon must face everyone and come to terms with his identity.",
           'genre' => 'Drama',
           'release_date' => '16 March 2018',
           'run_time' => '1h 50min',
           'poster' => 'posters/Love, Simon.jpg',
           'trailer_url' => 'https://youtu.be/ykHeGtN4m94'
        ]);
        DB::table('movies')->insert([
           'name' => 'Black Panther',
           'studio_id' => 7,
           'director' => 'Ryan Coogler',
           'description' => "T'Challa, heir to the hidden but advanced kingdom of Wakanda, must step forward to lead his people into a new future and must confront a challenger from his country's past.",
           'genre' => 'Action',
           'release_date' => '16 February 2018',
           'run_time' => '2h 14min',
           'poster' => 'posters/Black Panther.jpg',
           'trailer_url' => 'https://youtu.be/xjDjIWPwcPU'
        ]);
        DB::table('movies')->insert([
           'name' => 'The Shape of Water',
           'studio_id' => 8,
           'director' => 'Guillermo del Toro',
           'description' => "At a top secret research facility in the 1960s, a lonely janitor forms a unique relationship with an amphibious creature that is being held in captivity.",
           'genre' => 'Fantasy',
           'release_date' => '22 December 2017',
           'run_time' => '2h 3min',
           'poster' => 'posters/The Shape of Water.jpg',
           'trailer_url' => 'https://youtu.be/XFYWazblaUA'
        ]);
        DB::table('movies')->insert([
           'name' => 'A Wrinkle in Time',
           'studio_id' => 1,
           'director' => 'Ava DuVernay',
           'description' => "After the disappearance of her scientist father, three peculiar beings send Meg, her brother, and her friend to space in order to find him.",
           'genre' => 'Adventure',
           'release_date' => '9 March 2018',
           'run_time' => '1h 49min',
           'poster' => 'posters/A Wrinkle in Time.jpg',
           'trailer_url' => 'https://youtu.be/UhZ56rcWwRQ'
        ]);
        DB::table('movies')->insert([
           'name' => 'Ready Player One',
           'studio_id' => 2,
           'director' => 'Steven Spielberg',
           'description' => "When the creator of a virtual reality world called the OASIS dies, he releases a video in which he challenges all OASIS users to find his Easter Egg, which will give the finder his fortune.",
           'genre' => 'Action',
           'release_date' => '29 March 2018',
           'run_time' => '2h 20min',
           'poster' => 'posters/Ready Player One.jpg',
           'trailer_url' => 'https://youtu.be/cSp1dM2Vj48'
        ]);
        DB::table('movies')->insert([
           'name' => 'Jumanji: Welcome to the Jungle',
           'studio_id' => 3,
           'director' => 'Jake Kasdan',
           'description' => "Four teenagers are sucked into a magical video game, and the only way they can escape is to work together to finish the game.",
           'genre' => 'Adventure',
           'release_date' => '20 December 2017',
           'run_time' => '1h 59min',
           'poster' => 'posters/Jumanji- Welcome to the Jungle.jpg',
           'trailer_url' => 'https://youtu.be/2QKg5SZ_35I'
        ]);
        DB::table('movies')->insert([
           'name' => 'Avengers: Infinity War',
           'studio_id' => 7,
           'director' => 'Anthony Russo, Joe Russo',
           'description' => "The Avengers and their allies must be willing to sacrifice all in an attempt to defeat the powerful Thanos before his blitz of devastation and ruin puts an end to the universe.",
           'genre' => 'Action',
           'release_date' => '27 April 2018',
           'run_time' => '2h 29min',
           'poster' => 'posters/Avengers- Infinity War.jpg',
           'trailer_url' => 'https://youtu.be/6ZfuNTqbHE8'
        ]);
        DB::table('movies')->insert([
           'name' => "To All the Boys I've Loved Before",
           'studio_id' => 9,
           'director' => 'Susan Johnson',
           'description' => "A teenage girl's secret love letters are exposed and wreak havoc on her love life.",
           'genre' => 'Romance',
           'release_date' => '17 August 2018',
           'run_time' => '1h 39min',
           'poster' => "posters/To All the Boys I've Loved Before.jpg",
           'trailer_url' => 'https://youtu.be/555oiY9RWM4'
        ]);

    }
}
