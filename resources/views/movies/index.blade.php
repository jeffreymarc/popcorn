@extends('layouts.master')
@section('title')
    Popcorn
@endsection

@section('content')
	
	<div id="page">

	
	<div class="page-inner">

	
	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(images/img_2.jpg)">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					

					<div class="row row-mt-15em">

						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Create | Rate | Review | Follow</span>
							<h1>Popcorn Reviews</h1>	
						</div>
						
					</div>
							
					
				</div>
			</div>
		</div>
	</header>
	
	
	<div id="gtco-features" class="border-bottom">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading animate-box">
					<h2>Movies Featured</h2>
					<p>The movies below are movies created by our users. Feel free to browse all of them to make a list of your next movie to watch!</p>
				</div>
			</div>
			<div class="row">
			    @foreach($movies as $movie)
    				<div class="col-md-3 col-sm-6">
    					<div class="feature-center animate-box" data-animate-effect="fadeIn">
    					    <div class="card" style="width: 24rem; height: 40em;">
        					    <p><a href="/movie/{{$movie->id}}"><img class="card-img-top" src="{{$movie->poster}}" style="width: 238px; height: 354px;"></p></a>
        						<h3>{{$movie->name}}</h3>
        						<p class="card-text">{{$movie->description}}</p>
    						</div>
    					</div>
    				</div>
    			@endforeach
			</div>
		</div>
	</div>

	<div id="gtco-subscribe">
		<div class="gtco-container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2>Event</h2>
					<p>$8 Student Monday in any Event Cinemas nationwide.</p>
				</div>
			</div>
		</div>
	</div>

	
@endsection