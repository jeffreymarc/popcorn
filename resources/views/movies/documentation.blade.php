@extends('layouts.master')
@section('title')
    Documentation
@endsection

@section('content')
	
	<div id="page">

	
	<div class="page-inner">

	
	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(/images/img_2.jpg)">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					

					<div class="row row-mt-15em">

						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Create | Rate | Review | Follow</span>
							<h1>Documentation for Popcorn</h1>	
						</div>
						
					</div>
							
					
				</div>
			</div>
		</div>
	</header>
	
	
	<div id="gtco-features" class="border-bottom">
		<div class="gtco-container">
			<div class="row">
				<h2>Completed:</h2>
				<ul>
					<li>Migration/seeding for tables</li>
					<li>User registration</li>
					<li>User login but problems with redirection</li>
					<li>User logout</li>
					<li>Creating new items</li>
					<li>Create reviews</li>
					<li>List all items</li>
					<li>Details page</li>
					<li>Order reviews by highest rating or creation date</li>
					<li>Update items and reviews</li>
					<li>Delete items and review</li>
					<li>Input validation</li>
					<li>Access control front and back end</li>
					<li>Pagination</li>
					<li>Image</li>
					<li>Like/dislike</li>
					<li>Follow/unfollow</li>
					<li>Recommendation but partial only and does not work as intended.</li>
				</ul>
			</div>
		</div>
	</div>

	<div id="gtco-subscribe">
		<div class="gtco-container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2>Event</h2>
					<p>$8 Student Monday in any Event Cinemas nationwide.</p>
				</div>
			</div>
		</div>
	</div>

	
@endsection