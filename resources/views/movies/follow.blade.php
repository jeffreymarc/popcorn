@extends('layouts.master')
@section('title')
    Dashboard
@endsection

@section('content')
	
	<div id="page">

	
	<div class="page-inner">

	
	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(/images/img_2.jpg)">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					

					<div class="row row-mt-15em">

						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Reviews: {{$reviewCount}}| Followers: {{$followersCount}}| Following: {{$followingCount}}</span>
							<h1>{{$user}}</h1>	
						</div>
						
					</div>
							
					
				</div>
			</div>
		</div>
	</header>
	
	
	<div id="gtco-features" class="border-bottom">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading animate-box">
					@if(count($follows))
						<h2>Following</h2>
						@foreach($follows as $follow)
							@foreach($userList as $user)
								@if($follow->user_id_following == $user->id)
									<p>{{$user->name}}</p>
								@endif
							@endforeach
						@endforeach
					@else
						<h2>Following</h2>
						<p>Not following anyone</p>
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading animate-box">
					<h2>Feed</h2>
					@if(count($follows))
						@foreach($follows as $follow)
							@foreach($userList as $user)
								@if($follow->user_id_following == $user->id)
									@foreach($reviews as $review)
										@if($review->user_id == $user->id)
											<div class="col-md-12">
												<div class="price-box popular">
													<h2 class="pricing-plan">{{$user->name}}</h2>
													@foreach($movies as $movie)
														@if($movie->id == $review->movie_id)
															<p>{{$movie->name}}</p>
															@if ($movieID = $movie->id) @endif
														@endif
													@endforeach
													<hr>
													<h4>{{$review->rating}} out of 5</h4>
													<p>{{$review->reviewText}}</p>
													<a href="/movie/{{$movieID}}" class="btn btn-primary btn-sm">See Review</a>
												</div>
											</div>										
										@endif
									@endforeach
								@endif
							@endforeach
						@endforeach
					@else
						<p>Not following anyone</p>
					@endif
				</div>
			</div>
		</div>
	</div>

	<div id="gtco-subscribe">
		<div class="gtco-container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2>Event</h2>
					<p>$8 Student Monday in any Event Cinemas nationwide.</p>
				</div>
			</div>
		</div>
	</div>

	
@endsection