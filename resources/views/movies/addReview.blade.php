@extends('layouts.master')
@section('title')
    Review {{$movie->name}}
@endsection

@section('content')

	
	<div id="page">

	
	<div class="page-inner">
	
	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(/images/img_2.jpg)">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">

						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Create | Rate | Review | Follow</span>
							<h1>Add review</h1>	
						</div>
						
					</div>
					
				</div>
			</div>
		</div>
	</header>
	
	
	<div class="gtco-section border-bottom">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6 animate-box">
					<h3>Insert review for <b>{{$movie->name}}</b> below</h3>
                    @if (count($errors) > 0)
                        <div style="color: red;"> <ul>       
                        @foreach ($errors->all() as $error) 
                            <li>{{ $error }}</li>
                        @endforeach </ul>
                        </div>
                    @endif					
					<form method="POST" action="/movie/review/create/{{$movie->id}}" enctype="multipart/form-data">
					    {{csrf_field()}}
					    <input type="hidden" name="user_id" value="{{$user}}">
						
						<div class="row form-group">
							<div class="col-md-12">
								<label for="rating">Rating</label><br>
								<select name="rating" class="form-control" value={{old('rating')}}>
									<option value="5">5</option>
									<option value="4">4</option>
									<option value="3">3</option>
									<option value="2">2</option>
									<option value="1">1</option>
                                </select>
							</div>
						</div>	
						
						<div class="row form-group">
							<div class="col-md-12">
								<label for="reviewText">Review</label>
								<textarea rows="2" cols="25" class="form-control" placeholder="What did you think of the movie?" name="reviewText" style="max-width: 100%;max-height: 30em;">{{old('reviewText')}}</textarea>
								<!--<input type="text" name="description" class="form-control" value="{{old('decription')}}" placeholder="Summary/Sypnosis/Description of the movie">-->
								<!--<textarea name="decription" cols="15" rows="5" class="form-control" placeholder="Summary/Sypnosis/Description of the movie">{{old('description')}}</textarea>-->
							</div>
						</div>	
						
						<div class="row form-group">
							<div class="col-md-12">
								<label for="photos">Add movie images</label>
								<input type="file" class="form-control" name="photos[]" multiple>
							</div>
						</div>						
						
						<div class="form-group">
							<input type="submit" value="Add Review" class="btn btn-primary">
						</div>							
					</form>		
				</div>
				
				
				
				
				
				<div class="col-md-5 col-md-push-1 animate-box">
					
					<div class="gtco-contact-info">
						<br><br><br>
						<img src="/{{$movie->poster}}" width="400px">
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>

	</div>

	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="js/jquery.countTo.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<!-- Main -->
	<script src="js/main.js"></script>

@endsection