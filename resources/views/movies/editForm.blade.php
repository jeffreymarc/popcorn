@extends('layouts.master')
@section('title')
    Edit {{$movie->name}}
@endsection

@section('content')
	<div id="page">

	
	<div class="page-inner">
	
	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(/images/img_2.jpg)">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">

						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Create | Rate | Review | Follow</span>
							<h1>Edit {{$movie->name}}</h1>	
						</div>
						
					</div>
					
				</div>
			</div>
		</div>
	</header>
	
	
	<div class="gtco-section border-bottom">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-7 animate-box">
					<h3>Insert movie information below</h3>
                    @if (count($errors) > 0)
                        <div style="color: red;"> <ul>       
                        @foreach ($errors->all() as $error) 
                            <li>{{ $error }}</li>
                        @endforeach </ul>
                        </div>
                    @endif					
					<form method="POST" action="/movie/{{$movie->id}}" enctype="multipart/form-data">
					    {{csrf_field()}}
					    {{method_field('PUT')}}
						<div class="row form-group">
							<div class="col-md-12">
								<label for="name">Name</label>
								<input type="text" name="name" class="form-control" value="{{$movie->name}}" placeholder="Movie name">
							</div>
						</div>
						
						<div class="row form-group">
							<div class="col-md-12">
								<label for="studio">Studio</label><br>
								<select name="studio" class="form-control">
                                    @foreach ($studios as $studio)
                                        @if($studio->id == $movie->studio_id)
                                            <option value="{{$studio->id}}" selected="selected">{{$studio->name}}</option> 
                                        @else
                                            <option value="{{$studio->id}}">{{$studio->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-12">
								<label for="description">Description</label>
								<input type="text" name="description" class="form-control" value="{{$movie->description}}" placeholder="Summary/Sypnosis/Description of the movie">
								<!--<textarea name="decription" cols="15" rows="5" class="form-control" placeholder="Summary/Sypnosis/Description of the movie">{{old('description')}}</textarea>-->
							</div>
						</div>

						
						<div class="row form-group">
							<div class="col-md-12">
								<label for="director">Director</label>
								<input type="text" name="director" class="form-control" value="{{$movie->director}}" placeholder="Director of the movie">
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-12">
								<label for="genre">Genre</label>
								<input type="text" name="genre" class="form-control" value="{{$movie->genre}}" placeholder="Genre of the movie">
							</div>
						</div>
						
						<div class="row form-group">
							<div class="col-md-12">
								<label for="runtime">Run time</label>
								<input type="text" name="runtime" class="form-control" value="{{$movie->run_time}}" placeholder="e.g. 2h 30min">
							</div>
						</div>						
						
						<div class="row form-group">
							<div class="col-md-12">
								<label for="release date">Release Date</label>
								<input type="date" class="form-control" name="release_date" value="{{$movie->date}}">
							</div>
						</div>
						
						<div class="row form-group">
							<div class="col-md-12">
								<label for="URL">Youtube Trailer Link</label>
								<input type="URL" class="form-control" name="trailer_url" value="{{$movie->trailer_url}}">
							</div>
						</div>						

						<div class="row form-group">
							<div class="col-md-12">
								<label for="image">Poster</label>
								<input type="file" class="form-control" name="image" value="{{$movie->poster}}">
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-12">
								<label for="photos">Add movie images</label>
								<input type="file" class="form-control" name="photos[]" multiple>
							</div>
						</div>							
						
						<div class="form-group">
							<input type="submit" value="Add Movie" class="btn btn-primary">
						</div>						
					</form>		
				</div>
				<div class="col-md-5 col-md-push-1 animate-box">
					
					<div class="gtco-contact-info">
						<br><br><br>
						<img src="/{{$movie->poster}}" width="400px">
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>

	</div>

	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="js/jquery.countTo.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<!-- Main -->
	<script src="js/main.js"></script>

@endsection