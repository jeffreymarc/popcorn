@extends('layouts.master')
@section('title')
    {{$movie->name}}
@endsection

@section('content')
	<div id="page">
	<div class="page-inner">
	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url('/{{$movie->poster}}'); background-position: center;">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					

					<div class="row row-mt-15em">

						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Reviews for</span>
							<h1>{{$movie->name}}</h1>	
						</div>
						
					</div>
							
					
				</div>
			</div>
		</div>
	</header>
	<div class="gtco-section border-bottom">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-5 text-left gtco-heading">
					<h2>{{$movie->name}}</h2>
					<p>{{$movie->studio->name}} | {{$movie->genre}}</p>
				</div>
				<div class="col-md-7 text-right gtco-heading">
					@if(!Auth::guest())
						@if($user->type == 'moderator')
							<a href="/movie/review/create/{{$movie->id}}" class="btn btn-default btn-sm">Add Review</a>
							<a href="/movie/{{$movie->id}}/edit" class="btn btn-default btn-sm">Edit</a>
							<form style="display: inline-block;" action="/movie/{{$movie->id}}" method="POST">
								{{ method_field('DELETE') }}
	    						{{ csrf_field() }}
							    <button class="btn btn-default btn-sm">Delete</button>
							</form>
						@endif
						@if($user->type == 'regular')
							<a href="/movie/review/create/{{$movie->id}}" class="btn btn-default btn-sm">Add Review</a>
						@endif
					@endif
				</div>				
			</div>
			<div class="row">
				<div class="col-md-5">
					<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
						<span class="icon">
							<i>>>></i>
						</span>							
						<div class="feature-copy">
							<h3>Description</h3>
							<p>{{$movie->description}}</p>
						</div>
					</div>

					<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
						<span class="icon">
							<i>>>></i>
						</span>							
						<div class="feature-copy">
							<h3>Director</h3>
							<p>{{$movie->director}}</p>
						</div>
					</div>

					<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
						<span class="icon">
							<i>>>></i>
						</span>							
						<div class="feature-copy">
							<h3>Release date</h3>
							<p>{{$movie->release_date}}</p>
						</div>
					</div>

					<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
						<span class="icon">
							<i>>>></i>
						</span>						
						<div class="feature-copy">
							<h3>Run time</h3>
							<p>{{$movie->run_time}}</p>
						</div>
					</div>
					
					<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
						<div class="feature-copy">
							<h3>Trailer</h3>
							<a href="{{$movie->trailer_url}}" class="btn btn-primary btn-sm">Watch Trailer</a>
						</div>
					</div>					
					
				</div>
				<div class="col-md-7 macbook-wrap animate-box" data-animate-effect="fadeInRight">
					<img src="/{{$movie->poster}}" alt="Movie Poster" style="width:25em;" align="right">
				</div>
			</div>
		</div>
		
	</div>
	@if(count($images) > 0)
	<div class="gtco-section">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2>Images supplied by users</h2>
				</div>
			</div>
			<div class="row">
				@if(count($images) > 0)
					@foreach($images as $image)
						<div class="col-lg-4 col-md-4 col-sm-6">
							<a href="/{{$image->path}}" class="fh5co-project-item image-popup">
								<figure>
									<div class="overlay"><i class="ti-zoom-in"></i></div>
									<img src="/{{$image->path}}" class="img-responsive" style="width:335px; height: 250px">
								</figure>
								<div class="fh5co-text">
									<h2>{{$image->uploader}}</h2>
								</div>
							</a>
						</div>
					@endforeach
				@endif
			</div>
		</div>
	</div>
	@endif
	
	@if(count($reviews) > 0)
	<div class="gtco-section">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2>Reviews</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-offset-2" style="margin-top: 0; margin-bottom: 5em;">
					<form action="/movie/{{$movie->id}}" method="POST">
		    			{{ csrf_field() }}
		    			<div class="col-md-12">
							<select name="order" class="row form-group" style="dispay: inline-block;" onchange="this.form.submit()">
								<option>Order by</option>
							  	<option value="bydate">Newest review</option>
							  	<option value="byrating">Highest review</option>
							</select>			    			
					    </div>
					</form>	
				</div>
			</div>

			<div class="row">
			
				<div class="col-md-8 col-md-offset-2">
					<ul class="fh5co-faq-list">
						@foreach($reviews as $review)
								<li class="animate-box">
									@if(Auth::check())
										@if ($likeCount = 0) @endif
										@if ($dislikeCount = 0) @endif
										@foreach($likes as $like)
											@if( $like['review_id'] == $review['id'] )
												@if($like->score == 1)
													@php $likeCount += 1; @endphp
												@else
													@php $dislikeCount += 1; @endphp
												@endif
											@endif
										@endforeach
										@if($likeCount < $dislikeCount)
											<div class="hideMe">
										@else
											<div>
										@endif
												<h2>{{$review->rating}} out of 5</h2>
												<h4>{{$review->reviewText}}</h4>
												<p style="color: #444444">{{$review->user->name}} | {{$review->created_at}}</p>										
												<small style="margin-top: 0;">
													<form style="display: inline-block;" action="/movie/{{$movie->id}}" method="POST">
														<input type="hidden" name="review_id" value="{{$review->id}}">
														<input type="hidden" name="score" value="like">
									    				{{ csrf_field() }}
													    <button style="background: none;border: none;"><a>Like ({{$likeCount}})</a></button>
													</form>														
													
													<form style="display: inline-block;" action="/movie/{{$movie->id}}" method="POST">
														<input type="hidden" name="review_id" value="{{$review->id}}">
														<input type="hidden" name="score" value="dislike">
									    				{{ csrf_field() }}
													    <button style="background: none;border: none;"><a>Dislike ({{$dislikeCount}})</a></button>
													</form>	
												@if($j = 0) @endif
												@if(count($unfollows) == 0)
														@if(Auth::id() != $review->user_id)
															<form style="display: inline-block;" action="/dashboard" method="POST">
																<input type="hidden" name="follow" value="follow">
																<input type="hidden" name="user_id" value="{{$review->user_id}}">
											    				{{ csrf_field() }}
															    <button style="background: none;border: none;"><a>Follow</a></button>
															</form>	
															@if($j = 1) @endif
														@endif
												@else
													@foreach($unfollows as $unfollow)
														@if(Auth::id() != $review->user_id AND $unfollow->user_id_following == $review->user_id)
															<form style="display: inline-block;" action="/dashboard" method="POST">
																<input type="hidden" name="follow" value="unfollow">
																<input type="hidden" name="user_id" value="{{$review->user_id}}">
											    				{{ csrf_field() }}
															    <button style="background: none;border: none;"><a>Unfollow</a></button>
															</form>	
															@if($j = 1) @endif
															@break
														@endif
													@endforeach
												@endif
												@if($j == 0)
													@foreach($unfollows as $unfollow)
														@if(Auth::id() != $review->user_id AND $unfollow->user_id_following != $review->user_id)
															<form style="display: inline-block;" action="/dashboard" method="POST">
																<input type="hidden" name="follow" value="follow">
																<input type="hidden" name="user_id" value="{{$review->user_id}}">
											    				{{ csrf_field() }}
															    <button style="background: none;border: none;"><a>Follow</a></button>
															</form>	
															@break
														@endif
													@endforeach	
												@endif
												@if(Auth::id() == $review->user_id OR Auth::user()->type == 'moderator')
													<a href="/movie/review/{{$movie->id}}/edit">Edit</a>
													<form style="display: inline-block;" action="/movie/review/{{$movie->id}}" method="POST">
														<input type="hidden" name="review_id" value="{{$review->id}}">
														{{ method_field('DELETE') }}
									    				{{ csrf_field() }}
													    <button style="background: none;border: none;"><a>Delete</a></button>
													</form>	
												@endif
												</small>
											</div>
									@else
										@if ($likeCount = 0) @endif
										@if ($dislikeCount = 0) @endif
										@foreach($likes as $like)
											@if( $like['review_id'] == $review['id'] )
												@if($like->score == 1)
													@php $likeCount += 1; @endphp
												@else
													@php $dislikeCount += 1; @endphp
												@endif
											@endif
										@endforeach
										@if($likeCount < $dislikeCount)
											<div class="hideMe">
										@else
											<div>
										@endif
												<h2>{{$review->rating}} out of 5</h2>
												<h4>{{$review->reviewText}}</h4>
												<p style="color: #444444">{{$review->user->name}} | {{$review->created_at}}</p>										
												<small style="margin-top: 0;">
													    <a href="/login">Like ({{$likeCount}})</a>
													    <a href="/login">Dislike ({{$dislikeCount}})</a>
												</small>
											</div>

									@endif
								</li>
						@endforeach
					</ul>
				</div>
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading" style="margin-bottom: 0;">{{$reviews->links()}}</div>
			</div>
		</div>
	</div>
	@endif
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
				<h2>More movies like {{$movie->name}}</h2>
			</div>
			@foreach($recommends as $recommend)
				<div class="col-md-4">
					<div class="price-box" style="margin-right: 5px; margin-left: 5px; height:35em;">
						<h2 class="pricing-plan" style="font-size: small">{{$recommend->name}}</h2>
						<p><img src="/{{$recommend->poster}}" style="width: 238px; height: 354px;"></p>
						<hr style="margin-top: 10px;">
						<a href="/movie/{{$recommend->id}}" class="btn btn-primary btn-sm">See Movie</a>
					</div>
				</div>			
			@endforeach
		</div>	
	</div>
	</div>
@endsection
{{-- Test --}}