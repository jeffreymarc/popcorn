<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    function movie() {
        return $this->belongsTo('App\Movie');
    }
    
    function user() {
        return $this->belongsTo('App\User');
    }    
}
