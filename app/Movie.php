<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    function studio() {
        return $this->belongsTo('App\Studio');
    }
    
    function photos() {
        return $this->hasMany('App\Photo');
    }
    
}
