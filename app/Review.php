<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    function movie() {
        return $this->belongsTo('App\Movie');
    }
    
    function user(){
        return $this->belongsTo('App\User');
    }  
    function likes() {
        return $this->hasMany('App\Like');
    }    
}
