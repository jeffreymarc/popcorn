<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Movie;
use App\Studio;
use App\Photo;
use App\Review;
use App\User;
use App\Like;
use App\Follow;
use Storage;
use Auth;
class MovieController extends Controller
{
    public function __construct() {
      //  $this->middleware('auth', ['except'=>'index']); # no need to log in to access  
       $this->middleware('auth', ['only'=>['edit', 'review', 'review_update']]);
    }    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = Movie::all();
        return view('movies.index')->with('movies', $movies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('movies.createForm')->with('studios', Studio::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'name'=> 'required | max: 50 | unique:movies',
           'studio'=> 'exists:studios,id',
           'description'=> 'required | max:255',
           'director'=> 'max: 50',
           'genre'=>'max:255',
           'run_time'=> 'max: 9',
           'image' => 'image|mimes:jpg,png,jpeg|max:5000'
        ]);
        if (empty($_FILES)) {
            $image_store = request()->file('image')->store('posters', 'public');
            
        }
        else{
            $image_store = 'posters/default.png';
        }
        $movie = new Movie();
        $movie->name = $request->name;
        $movie->studio_id = $request->studio;
        $movie->description = $request->description;
        $movie->poster = $image_store;
        // $photos->id = $movie->id;
        #if optional fields are empty
        if(! Input::get('director') ){
            $movie->director = 'No info';
        }
        else{
            $movie->director = $request->director;
        }
        if(! Input::get('genre') ){
            $movie->genre = 'No info';
        }
        else{
            $movie->genre = $request->genre;
        }
        if(! Input::get('run_time') ){
            $movie->run_time = 'No info';
        }
        else{
            $movie->run_time = $request->run_time;
        }         
        if(! Input::get('release_date') ){
            $movie->release_date = '01/01/2001';
        }
        else{
            $movie->release_date = $request->release_date;
        }  
        if(! Input::get('trailer_url') ){
            $movie->trailer_url = 'https://www.youtube.com/';
        }
        else{
            $movie->trailer_url = $request->trailer_url;
        }          
        $movie->save();
        // $photo = new Photo();
        // $photo->id = $movie->id;
        // $photo->movie_id = $movie->id;
        // $photo->path = $movie->image;
        // $photo->uploader = $movie->name;
        return redirect("/movie/$movie->id");        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $image = Photo::find($id); //find all photos where id is the movie id
        $movie = Movie::find($id); //find current movie
        //like
        if($request->score == 'like'){
            $check = Like::where([['movie_id', '=', $id], ['user_id', '=', Auth::id()], ['review_id', '=', $request->review_id]])->get(); //checks if user has already liked/disliked
            if(count($check) > 0){ //checks if user has liked it
                if($check[0]->score == 1){
                    $check = Like::where([['movie_id', '=', $id], ['user_id', '=', Auth::id()], ['review_id', '=', $request->review_id]]);
                    $check->delete(); //cancelling the like
                }
                else{
                    //delete dislike and add a new like
                    $check = Like::where([['movie_id', '=', $id], ['user_id', '=', Auth::id()], ['review_id', '=', $request->review_id]]);
                    $check->delete();
                    $like = new Like();
                    $like->user_id = Auth::id();
                    $like->review_id = $request->review_id;
                    $like->score = 1;
                    $like->movie_id = $id;
                    $like->save();
                }
            }  
            else{
                $like = new Like();
                $like->user_id = Auth::id();
                $like->review_id = $request->review_id;
                $like->score = 1;
                $like->movie_id = $id;
                $like->save();
            }
        }
        
        if($request->score == 'dislike'){
            $check = Like::where([['movie_id', '=', $id], ['user_id', '=', Auth::id()], ['review_id', '=', $request->review_id]])->get(); //checks if user has already liked/disliked
            if(count($check) > 0){ //checks if user has liked it
                if($check[0]->score == -1){
                    $check = Like::where([['movie_id', '=', $id], ['user_id', '=', Auth::id()], ['review_id', '=', $request->review_id]]);
                    $check->delete(); //cancelling the like
                }
                else{
                    //delete dislike and add a new like
                    $check = Like::where([['movie_id', '=', $id], ['user_id', '=', Auth::id()], ['review_id', '=', $request->review_id]]);
                    $check->delete();
                    $like = new Like();
                    $like->user_id = Auth::id();
                    $like->review_id = $request->review_id;
                    $like->score = -1;
                    $like->movie_id = $id;
                    $like->save();
                }
            }  
            else{
                $like = new Like();
                $like->user_id = Auth::id();
                $like->review_id = $request->review_id;
                $like->score = -1;
                $like->movie_id = $id;
                $like->save();
            }
        }
        
        
        if($request->order == 'byrating'){
            $reviews = Review::where('movie_id', $id)->orderBy('rating', 'DESC')->paginate(5);
            $likes = Like::where('movie_id', $id)->get();
        }
        else{
            $reviews = Review::where('movie_id', $id)->orderBy('created_at', 'DESC')->paginate(5);
            $likes = Like::where('movie_id', $id)->get();
        }
        if (!empty($image)){
            $image = Photo::where('id', $id)->get();
        }
     
        if (Auth::check())
        {
            $user = Auth::user();
        }
        else{
            $user = 0;
        }
        
        $unfollows = Follow::where('user_id_current',  Auth::id())->get();
        
        // if(count($unfollows)==0){
        //     $follow = new Follow;
        //     $follow->user_id_current = Auth::id();
        //     $follow->user_id_following = -1;
        //     $follow->save();
        // }
        // $unfollows = Follow::where('user_id_current',  Auth::id())->get();

        $genre = Movie::all('genre');
        $recommend = Movie::where('genre', 'LIKE', '%'.$movie->genre.'%')->where('id', '!=', $id)->get();
        return view('movies.show')->with('movie', $movie)->with('images', $image)->with('reviews', $reviews)->with('user', $user)->with('likes', $likes)
        ->with('unfollows', $unfollows)->with('recommends', $recommend);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check()){
            $user = Auth::user();
            if($user->type == 'regular'){
                return back();
            }
        }
        $movie = Movie::find($id);
        return view('movies.editForm')->with('movie', $movie)->with('studios', Studio::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $movie = Movie::find($id);
        $image_store = $movie->poster;
        $movie->delete();
        $this->validate($request, [
           'name'=> 'required | max: 50 | unique:movies',
           'studio'=> 'exists:studios,id',
           'description'=> 'required | max:255',
           'director'=> 'max: 50',
           'genre'=>'max:255',
           'run_time'=> 'max: 9',
           'image' => 'image|mimes:jpg,png,jpeg|max:5000',
           'photos[]' => 'image|mimes:jpg,png,jpeg|max:5000'
        ]);
        
        #check if file upload is empty. If it is, give default image.
        $movie = new Movie();
        $movie->name = $request->name;
        $movie->studio_id = $request->studio;
        $movie->description = $request->description;
        if (!empty($request->image)) {
            $image_store = request()->file('image')->store('posters', 'public');
        }

        $movie->poster = $image_store;
        // $photos->id = $movie->id;
        #if optional fields are empty
        if(! Input::get('director') ){
            $movie->director = 'No info';
        }
        else{
            $movie->director = $request->director;
        }
        if(! Input::get('genre') ){
            $movie->genre = 'No info';
        }
        else{
            $movie->genre = $request->genre;
        }
        if(! Input::get('run_time') ){
            $movie->run_time = 'No info';
        }
        else{
            $movie->run_time = $request->run_time;
        }         
        if(! Input::get('release_date') ){
            $movie->release_date = '01/01/2001';
        }
        else{
            $movie->release_date = $request->release_date;
        }
        if(! Input::get('trailer_url') ){
            $movie->trailer_url = 'https://www.youtube.com/';
        }
        else{
            $movie->trailer_url = $request->trailer_url;
        }          
        $movie->save();
        if ($photos=$request->file('photos')) {
            foreach ($photos as $photo)
            {
                $path = $photo->store('posters', 'public');
                $p = new Photo();
                $p->id = $movie->id;
                $p->movie_id = $movie->id;
                $p->path = $path;
                $p->uploader = Auth::id();
                $p->save();
            } 
        }        
        // $photo = new Photo();
        // $photo->id = $movie->id;
        // $photo->movie_id = $movie->id;
        // $photo->path = $movie->image;
        // $photo->uploader = $movie->name;
        return redirect("/movie/$movie->id");        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        if(Review::where('movie_id', '=', $id)){
            $reviews = Review::where('movie_id', '=', $id)->get();
            foreach ($reviews as $review){
                $review->delete();
            }            
        }

        $movie = Movie::find($id);
        $movie->delete();
        return redirect('/movie');
    }
    
    // public function review($id)
    // {
    //     if (Auth::check())
    //     {
    //         $user = Auth::id();
    //         $movie = Movie::find($id);
    //         //$email = Auth::user()->email;
    //         return view('movies.addReview')->with('movie', $movie)->with('user', $user);
    //     }
    //     else{
    //         $movie = Movie::find($id);
    //         return view('movies.addReview')->with('movie', $movie);
    //     }

    // }
    
    // public function review_store(Request $request, $id)
    // {
    //     $this->validate($request, [
    //         'user_id'=> 'unique:reviews,user_id,NULL,id,movie_id,'.$id,
    //         'reviewText' => 'required | max:255',
    //     ]);      
        
    //     $review = new Review();
    //     $review->user_id = $request->user_id;
    //     $review->movie_id = $id;
    //     $review->rating = $request->rating;
    //     $review->reviewText = $request->reviewText;
    //     $review->save();
    //     return redirect("/movie/$id");  
    // }
    
    // public function review_update($id)
    // {
    
    //     $movie = Movie::find($id);
    //     if (Auth::check())
    //     {
    //         if(Auth::user()->type == 'regular'){
    //             $reviews = Review::where([['movie_id', '=', $id], ['user_id', '=', Auth::id()]])->get();
    //             if(count($reviews) > 0){
    //                 return view('movies.editReview')->with('movie', $movie)->with('review', $reviews);
    //             }
    //             else{
    //                 return redirect('movie/'.$id.'/review');
    //             }
    //         }
    //         if(Auth::user()->type == 'moderator'){
    //             $reviews = Review::where('movie_id', '=', $id)->get();
    //             return view('movies.editReview')->with('movie', $movie)->with('review', $reviews);
    //         }            
    //     }

    // }
    
    // public function review_store_update(Request $request, $id)
    // {
    //     $reviews = Review::where([['movie_id', '=', $id], ['user_id', '=', Auth::id()]])->get();
    //     $reviews->delete();
    //     $this->validate($request, [
    //         'user_id'=> 'unique:reviews,user_id,NULL,id,movie_id,'.$id,
    //         'reviewText' => 'required | max:255',
    //     ]);      
        
    //     $review = new Review();
    //     $review->user_id = $request->user_id;
    //     $review->movie_id = $id;
    //     $review->rating = $request->rating;
    //     $review->reviewText = $request->reviewText;
    //     $review->save();
    //     return redirect("/movie/$id");  
    // }    
    
    public function destroy_review($id)
    {
        dd("here");
        // if(Review::where('movie_id', '=', $id)){
        //     $reviews = Review::where('movie_id', '=', $id)->get();
        //     foreach ($reviews as $review){
        //         $review->delete();
        //     }            
        // }

        // $movie = Movie::find($id);
        // $movie->delete();
        // return redirect('/movie');
    }    
    
}
