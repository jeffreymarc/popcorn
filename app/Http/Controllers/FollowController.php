<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use App\Studio;
use App\Photo;
use App\Review;
use App\User;
use App\Like;
use App\Follow;
use Auth;
class FollowController extends Controller
{
    
    public function __construct() {
      //  $this->middleware('auth', ['except'=>'index']); # no need to log in to access  
       $this->middleware('auth', ['only'=>['index']]);
    }     
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $follows = Follow::where('user_id_current',Auth::id())->get();
        $followersCount = Follow::where('user_id_current',Auth::id())->get()->count();
        $followingCount = Follow::where('user_id_following',Auth::id())->get()->count();
        $user = Auth::user()->name;
        $reviewCount = Review::where('user_id', Auth::id())->get()->count();
        $review = Review::all();
        $users = User::all();     
        $movies = Movie::all();
        

        return view('movies.follow')->with('follows', $follows)->with('followersCount', $followersCount)->with('reviews', $review)
        ->with('followingCount', $followingCount)->with('user', $user)->with('reviewCount', $reviewCount)->with('userList', $users)->with('movies', $movies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->follow == 'follow'){
            $follow = new Follow();
            $follow->user_id_current = Auth::id();
            $follow->user_id_following = $request->user_id;

            $follow->save();
            
            return redirect()->back();
        }
        if($request->follow == 'unfollow'){
            $follow = Follow::where([['user_id_current', Auth::id()], ['user_id_following', $request->user_id]]);
            $follow->delete();
            
            return redirect()->back();
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
