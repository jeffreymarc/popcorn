<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use App\Studio;
use App\Photo;
use App\Review;
use App\User;
use Storage;
use Auth;
class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        if (Auth::check())
        {
            $user = Auth::id();
            $movie = Movie::find($id);
            //$email = Auth::user()->email;
            return view('movies.addReview')->with('movie', $movie)->with('user', $user);
        }
        else{
            $movie = Movie::find($id);
            return view('movies.addReview')->with('movie', $movie);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'user_id'=> 'unique:reviews,user_id,NULL,id,movie_id,'.$id,
            'reviewText' => 'required | max:255',
            'photos[]' => 'image|mimes:jpg,png,jpeg|max:5000',
        ]);      
        
        $review = new Review();
        $review->user_id = $request->user_id;
        $review->movie_id = $id;
        $review->rating = $request->rating;
        $review->reviewText = $request->reviewText;
        $review->save();
        if ($photos=$request->file('photos')) {
            foreach ($photos as $photo)
            {
                $path = $photo->store('posters', 'public');
                $p = new Photo();
                $p->id = $id;
                $p->movie_id = $id;
                $p->path = $path;
                $p->uploader = Auth::user()->name;
                $p->save();
            } 
        }         
        return redirect("/movie/$id");  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $movie = Movie::find($id);
        if (Auth::check())
        {
            if(Auth::user()->type == 'regular'){
                $reviews = Review::where([['movie_id', '=', $id], ['user_id', '=', Auth::id()]])->get();
                if(count($reviews) > 0){
                    return view('movies.editReview')->with('movie', $movie)->with('review', $reviews);
                }
                else{
                    return redirect('movie/'.$id.'/review');
                }
            }
            if(Auth::user()->type == 'moderator'){
                $reviews = Review::where('movie_id', '=', $id)->get();
                return view('movies.editReview')->with('movie', $movie)->with('review', $reviews);
            }            
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reviews = Review::where([['movie_id', '=', $id], ['user_id', '=', Auth::id()]])->get();
        foreach($reviews as $review){
            $review->delete();
        }
        $this->validate($request, [
            'user_id'=> 'unique:reviews,user_id,NULL,id,movie_id,'.$id,
            'reviewText' => 'required | max:255',
        ]);      
        
        $review = new Review();
        $review->user_id = Auth::id();
        $review->movie_id = $id;
        $review->rating = $request->rating;
        $review->reviewText = $request->reviewText;
        $review->save();
        return redirect("/movie/$id"); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $review_id = $request->review_id;
        if(Review::where([['movie_id', '=', $id], ['id', '=', $review_id]])){
            $reviews = Review::where([['movie_id', '=', $id], ['id', '=', $review_id]])->first();
            $reviews->delete();

        }

        // $movie = Movie::find($id);
        // $movie->delete();
        return redirect('/movie/'.$id);
    }
}
