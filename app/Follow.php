<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    function user1() {
        return $this->belongsTo('App\User');
    }
    
    function user2() {
        return $this->belongsTo('App\User');
    }    
}
