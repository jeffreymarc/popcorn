<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Movie;
use App\Studio;
Route::get('movie/{id}/review', 'MovieController@review');

Route::post('movie/{id}/review', 'MovieController@review_store');
// Route::get('movie/{id}/review/edit', 'MovieController@review_update');
// Route::post('movie/{id}/review/edit', 'MovieController@review_store_update');
Route::get('movie/{id}/review', 'ReviewController@destroy');
Route::resource('movie', 'MovieController');
Route::post('movie/{id}', 'MovieController@show');


Route::resource('movie/review', 'ReviewController');
Route::get('movie/review/create/{id}', 'ReviewController@create');
Route::post('movie/review/create/{id}', 'ReviewController@store');
Route::post('movie/review/create/{id}/edit', 'ReviewController@update');


Route::resource('dashboard', 'FollowController');

Route::get('/documentation', function () {
        return view('movies.documentation');
});


Route::get('/', function () {
        return redirect('/movie');
});


Auth::routes();



Route::get('/home', 'HomeController@index')->name('home');
